// 3-rd part libs
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

// components
import Header from './Header/Header';
import Container from '@material-ui/core/Container';
import Landing from './Landing/Landing';

const App = ({ store }) =>
    <Provider store={store}>
        <Router>
            <Header />
            <Container>
                <Switch>
                    <Route exact path="/">
                        <Landing />
                    </Route>
                    <Redirect to="/"/>
                </Switch>
            </Container>
        </Router>;
    </Provider>;

App.propTypes = {
    store: PropTypes.object.isRequired
};

export default App;
