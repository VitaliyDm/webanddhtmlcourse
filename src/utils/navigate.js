export default function navigate(path, history) {
    history.push(path);
}
