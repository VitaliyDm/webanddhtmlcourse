// constants
import CafeList from '../constants/actionTypes/CafeList';

const initialState = {
    list:[]
};

function cafeListReducer(state = initialState, action) {
    switch (action.type) {
    case CafeList.LOAD_LIST:
        return state;
    default:
        return state;
    }
}

export default cafeListReducer;
