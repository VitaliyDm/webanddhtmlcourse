// constants
import CafeList from '../../constants/actionTypes/CafeList';

export default function getList() {
    return ({
        type: CafeList.LOAD_LIST,
        data: 'some data'
    });
}
