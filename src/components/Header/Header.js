// 3-rd part libs
import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';

// components
import Button from '@material-ui/core/Button';

// utils
import navigate from '../../utils/navigate';

// images
import logo from '../../assetes/logo.png';

// styles
import styles from './Header.css';

const Header = ({
    authenticated = false
}) => {
    const history = useHistory();

    return (
        <div className={classnames('headerShadow', 'headerMain')}>
            <div className='logo' onClick={() => navigate('/', history)}>
                <img src={logo} alt='logo' className='logoImage' />
                <span className='logoText'>
                    Book Cafe
                </span>
            </div>
            <div className='controlMenu'>
                {
                    authenticated ?
                        <div>
                            <Button>

                            </Button>
                        </div> :
                        <div>

                        </div>
                }
            </div>
        </div>);
};

Header.propTypes = {
    authenticated: PropTypes.bool
};

export default Header;
