// 3-rd part libs
import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';

// styles
import './index.css';

// components
import App from './components/App';

// utils
import * as serviceWorker from './utils/serviceWorker';

// reducers
import rootReducer from './reducers';

const store = createStore(rootReducer);

ReactDOM.render(<App store={store}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
