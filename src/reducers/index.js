// 3-rd part libs
import { combineReducers } from 'redux';

// reducers
import cafeListReducer from './cafeList';

const rootReducer = combineReducers({
    cafeList: cafeListReducer
});

export default rootReducer;
